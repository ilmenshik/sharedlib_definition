package com.local.ucs

import com.local.common.ParentClazz

class UserNotificationsClazz extends ParentClazz {
    UserNotificationsClazz(def pipeline) {
        super(pipeline)
    }

    List<String> getAdditionalRecipients(String branchType) {
        _notImplemented()
        return []
    }

    String sendBuildResults(String branchType, String builduserEmail) {
        Set<String> emailRecipients = [builduserEmail]
        List<String> addRecipients = getAdditionalRecipients(branchType)
        if (addRecipients) {
            echo "Additional email recipients: ${addRecipients}"
            emailRecipients.addAll(addRecipients)
        }
        echo "Send email: ${emailRecipients.join(',')}"
    }
}