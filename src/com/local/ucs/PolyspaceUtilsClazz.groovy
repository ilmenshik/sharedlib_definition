package com.local.ucs

import com.local.common.ParentClazz

class PolyspaceUtilsClazz extends ParentClazz {
    String polyspaceAccessAddress
    String polyspaceAccessPort = '9443'
    Integer maxParallelBugFinder = 1
    Integer maxParallelCodeProver = 1

    PolyspaceUtilsClazz(def pipeline) {
        super(pipeline)
    }

    void upload(String accessPath, String itemName, String reportsDir) {
        echo "[INFO] Done polyspace-access tool"
    }
}