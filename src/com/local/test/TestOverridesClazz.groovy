package com.local.test

import com.local.common.ParentClazz

class TestOverridesClazz extends ParentClazz {
    String agentDataFileLinux

    TestOverridesClazz(def pipeline) {
        super(pipeline)
    }

    Boolean isProductionJob() {
        return pipeline.env.testParentOverride == 'true'
    }

    Boolean testOverride() {
        return this.agentDataFileLinux == 'agentDataFileLinuxOverride'
    }

    String hierarhyTest() {
        return hierarhyOverrideTest() == 'OverridedFromLocal'
    }

    String hierarhyOverrideTest() {
        return "TestOverridesClazz"
    }
}
