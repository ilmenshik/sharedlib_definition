package com.local.common

abstract class SettingsClazz {
    protected def pipeline
    Integer buildWaitTime = 0
    String gitCredID

    SettingsClazz(def pipeline) {
        this.pipeline = pipeline
    }

    void start() {
        this.buildWaitTime = getCurrentTimeStampSeconds() - getStartTime()
    }

    Integer getWaitTime() {
        return this.buildWaitTime
    }

    Integer getBuildTime() {
        return getCurrentTimeStampSeconds() - getStartTime() - this.buildWaitTime
    }
}
