package com.local.common

abstract class ParentClazz {
    protected def pipeline
    String agentDataFileLinux = '/ucs-linux-agent.inf'

    ParentClazz(def pipeline) {
        this.pipeline = pipeline
    }

    void _notImplemented() {
        String methodName = ""
        def traces = new Throwable().getStackTrace()
        int i = 0
        for (def trace in traces) {
            i += 1
            if (trace.methodName == '_notImplemented') {
                methodName = traces[i].methodName
                break
            }
        }
        echo "[WARNING] Skipped not implemented sharedlib method: $methodName()"
    }

    Boolean isProductionJob() {
        return env.JOB_NAME.endsWith('_prod')
    }

    void echo(def msg) {
        pipeline.echo(msg)
    }
}
